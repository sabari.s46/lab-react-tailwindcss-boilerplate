import React from "react";
import "./App.css";
import Body from "./components/Body";
import Footer from "./components/Footer";
import NavBar from "./components/NavBar";

function App() {
  return (
    <div className="h-screen w-screen flex flex-col">
      <nav>
        <NavBar />
      </nav>

      <main>
        <Body />
      </main>

      <footer className="mt-auto">
        <Footer />
      </footer>
    </div>
  );
}

export default App;
