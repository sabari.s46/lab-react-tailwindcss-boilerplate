function Footer() {
  return (
    <footer className="h-16 bg-gray-300 w-full flex justify-center content-center flex-wrap mt-auto">
      <div className="font-medium">&copy; 2021 Copyright: Kalvium</div>
    </footer>
  );
}

export default Footer;
