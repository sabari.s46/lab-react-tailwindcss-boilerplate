export default function Body() {
  return (
    <div className="mx-28">
      <div>
        {/* button */}
        <button className="bg-blue-500 text-white p-2 rounded my-5 text-xl font-semibold hover:shadow-lg">
          Buttton One
        </button>
      </div>
      <div className="border-red-500 border-2 p-2 bg-red-100">
        {/* alert */}
        <span className="text-red-600 font-bold mx-1 text-lg">Alert!</span>
        <span className="text-red-600 font-medium mx-1 text-lg">
          This is awesome!
        </span>
      </div>
      <div className="flex justify-center m-5">
        {/* card */}
        <div className="flex justify-center content-center flex-wrap shadow-md rounded h-24 p-2 m-2 px-10">
          <img
            src="https://media.licdn.com/dms/image/C560BAQEmI-b9h9lQ9Q/company-logo_200_200/0/1662977309233/kalvieducation_logo?e=2147483647&v=beta&t=5SHkVfvYHYtAF_5MxcVfP6N9ag2GCzYryCrFm1pxsQ8"
            alt="kalvium-logo"
            className="h-14 w-14"
          />
          <div className="px-2">
            <div className="font-semibold text-xl">Kalvium Store</div>
            <div className="text-gray-500 font-medium">
              You have a new course!
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
